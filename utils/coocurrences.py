import numpy as np
import pickle

def uniform_quantization(image, bits=4):
    levels = 2 ** bits
    scale = 256 // levels
    table = np.arange(0, 256, scale)
    return np.digitize(image, table) - 1


from numpy import array,zeros,max


def coocurrence0(M:array):
    temp=zeros((max(M)+1,max(M)+1))
    for i in range(max(M)+1): 
      for j in range(max(M)+1):
         for k in range(M.shape[0]):
            for u in range(M.shape[1]-1):
               if(M[k][u]==i and M[k][u+1]==j):
                  temp[i][j]+=1
                  
    return temp


def coocurrence45(M:array):
    temp=zeros((max(M)+1,max(M)+1))
    for i in range(max(M)+1): 
      for j in range(max(M)+1):
         for k in range(M.shape[0]-1):
            for u in range(M.shape[1]-1):
               if(M[k][u]==i and M[k+1][u+1]==j):
                  temp[i][j]+=1
                  
    return temp

def coocurrence90(M:array):
    temp=zeros((max(M)+1,max(M)+1))
    for i in range(max(M)+1): 
      for j in range(max(M)+1):
         for k in range(M.shape[0]-1):
            for u in range(M.shape[1]):
               if(M[k][u]==i and M[k+1][u]==j):
                  temp[i][j]+=1
                  
    return temp

def coocurrence135(M:array):
    temp=zeros((max(M)+1,max(M)+1))
    for i in range(max(M)+1): 
      for j in range(max(M)+1):
         for k in range(M.shape[0]-1):
            for u in range(M.shape[1]-1):
               if(M[k][u+1]==i and M[k+1][u]==j):
                  temp[i][j]+=1
                  
    return temp

def make_pred(x:np.array, classes = np.array(['ang', 'dis', 'fear', 'hap', 'neu', 'sad', 'sur'])):
    model = pickle.load(open('myModel.p','rb'))
    probabilities = model.predict_proba(x.reshape(1, -1))
    
    return classes[np.argmax(probabilities)]
import math
import os

from numpy import array

from PIL import Image

def moyenne (matrice):
    moyenne=0
    for j in range(len(matrice)):
        for i in range(len(matrice[0])):
            moyenne+=matrice[i][j]
    return moyenne/(len(matrice)*len(matrice[0]))           
# calcul de la variance d un indice
def variance (matrice):
    variance=0
    for i in range(len(matrice)):
        for j in range(len(matrice[0])):
            variance+=pow((i-moyenne(matrice)),2)*matrice[i][j]
    return variance

#Calculons l energie 
def Energie (matrice):
    energie=0
    for j in range(len(matrice[0])):
        for i in range(len(matrice)):
            energie+=pow(matrice[i][j],2)
    return energie

#Calculons l Inertie 
def Inertie(matrice):
    inertie=0
    for j in range(len(matrice[0])):
        for i in range(len(matrice)):
            inertie+=pow((i-j),2)*matrice[i][j]
    return inertie

# calculons l entropy

def entropy(matrice):
    entropy=0
    temp=0
    for j in range(len(matrice[0])):
        for i in range(len(matrice)):
            if matrice[i][j]!=0:
                entropy+=(-matrice[i][j]*math.log(matrice[i][j]))
    return entropy
#moyenne sur i
def moyenne_i(matrice):
    moyenne=0
    temp=0
    for i in range(len(matrice)):
        temp+=i
        for j in range(len(matrice[0])):
            moyenne+=temp*matrice[i][j]
    return moyenne
# moyenne sur j
def moyenne_j(matrice):
    moyenne=0
    temp=0
    for j in range(len(matrice[0])):
        temp+=j
        for i in range(len(matrice)):
            moyenne+=temp*matrice[i][j]
    return moyenne
# Moment differentiel inverse
def moment_diff_inv(matrice):
    moment=0
    for j in range(len(matrice[0])):
        for i in range(len(matrice)):
            moment+=(1/(1+pow((i-j),2)))*matrice[i][j]
    return moment
# moyenne locale sur les i

def ecart_loc_i(matrice,i):
    ecart=0
    temp=0
    for i in range(len(matrice)):
        temp+=pow(i-moyenne_i(matrice),2)
        for j in range(len(matrice[0])):
            ecart+=temp*matrice[i][j]
    return ecart

# calculons l ecart locale sur j
def ecart_loc_j(matrice,j):
    ecart=0
    temp=0
    for j in range(len(matrice[0])):
        temp=pow(j-moyenne_j(matrice),2)
        for i in range(len(matrice)):
            ecart=temp*matrice[i][j]
    return ecart       



# calcul du moment 
def moment(matrice):
    moment=0
    for i in range(len(matrice)):
        for j in range(len(matrice[0])):
            moment+=(i-j)*matrice[i][j]
    return moment
                    


def normalise(A:array, B:array)->array:
    for i in range(len(A)):
        B[i] =(B[i]-A[i][1])/(A[i][0]-A[i][1])
    return B


def toPgm(filename):
    if filename.endswith((".tiff", ".png", ".jpg")):

        # Chemin complet du fichier d'entrée
        input_path = filename

        # Chemin complet du fichier de sortie avec l'extension .pgm
        output_path = os.path.splitext(filename)[0] + ".pgm"

        # Ouvrir l'image d'entrée en utilisant PIL
        image = Image.open(input_path)

        # Convertir l'image en niveaux de gris
        image = image.convert("L")

        # Recupérer les dimensions de l'image
        width, height = image.size

        # Ouvrir le fichier de sortie en mode écriture
        with open(output_path, "w") as file:
            # Ecrire l'en-tête du fichier .pgm
            file.write("P2\n")
            file.write(f"# Converted from scratch\n")
            file.write(f"{width}  {height}\n")
            file.write("255\n")

            # Parcourir les pixels de l'image et les écrire dans le fichier .pgm
            for y in range(height):
                for x in range(width):
                    pixel_value = image.getpixel((x,y))
                    file.write(f"{pixel_value}\n")
                    
    return output_path
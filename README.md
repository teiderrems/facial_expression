# face_express_backend



## Getting started

requirements
python3

# commands

- create virtual environment
    python3 -m venv name_environment

- install dependencies
    python3 -m pip install -r requirements.txt

- run server

    python3 manage.py runserver

from django.db import models

# Create your models here.

class FaceImage(models.Model):
    face_image = models.ImageField(max_length=None)
    label = models.CharField(max_length=10,blank=True)

    def __str__(self):
        return self.face_image + ' ' + self.label

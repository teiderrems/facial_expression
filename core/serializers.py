from rest_framework import serializers
from .models import FaceImage

class FaceImageSerializer(serializers.ModelSerializer):

    face_image = serializers.ImageField()
    class Meta:
        model = FaceImage
        fields = ('face_image', 'label')

    def create(self,validated_data):
        current=FaceImage.objects.create(face_image=validated_data['face_image'])
        return current
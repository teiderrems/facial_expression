from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
from django.http.response import JsonResponse
from .serializers import FaceImageSerializer
from .models import FaceImage


from PIL import Image
from numpy import array
import utils.fonctions as func
import utils.coocurrences as coo

# Create your views here.

class FaceImageViewSet(viewsets.ModelViewSet):
    queryset =  FaceImage.objects.all()
    serializer_class = FaceImageSerializer

    # def create(self, request):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()

    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    
    # def retrieve(self, request, pk=None):
    #     image = get_object_or_404(self.queryset, pk=pk)
    #     serializer = FaceImageSerializer(image)
        
    #     return Response(serializer.data, status=status.HTTP_200_OK)
    
    # def update(self, request, pk=None):
    #     instance = self.get_object(pk=pk)
    #     serializer = self.get_serializer(instance, data=request.data, partial=True)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_update(serializer)

    #     return Response(serializer.data, status=status.HTTP_202_ACCEPTED)


def showResults(request):
    query=FaceImage.objects.last()

    print(query.face_image)
    mat=array(Image.open(func.toPgm(query.face_image)))
    mat=coo.uniform_quantization(mat)
    matrice=coo.coocurrence135(mat,4)
    vecteur=array([func.moyenne(matrice),func.variance(matrice),func.Energie(matrice),func.Inertie(matrice),func.entropy(matrice),func.moment_diff_inv(matrice),func.moment(matrice)])
    vec=array([[0.037053,0.019425],[1160.351453,724.303980],[2.670784,1.687822],[68.149601,12.459395],[15.100224,8.846326],[4.812715,3.441410],[11.796723,1.429379]])
    vecteur=func.normalise(vec,vecteur)
    
    return JsonResponse({"label":coo.make_pred(array(vecteur))})